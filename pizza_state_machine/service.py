from transitions import Machine

from pizza_state_machine.models import Order, User
from pizza_state_machine.state_machine_description import \
    PizzaStateMachineDescription, CONFIRMED


class PizzaServiceException(Exception):
    """
        Custom exception for PizzaService
    """
    pass


class PizzaService(object):
    """
        Manager for initialize state machine for Order object
    """

    state_message = {
        'initial': "Which pizza do you want? Big or small?",
        'pizza_selected': "How would you like to pay? By card or by cash?",
        'payment_selected': "Do you want {pizza_size} pizza, "
                            "payment - {payment_method}? Type yes or no",
        'success': "Thank you for order!Now you can order another pizza"
                   " using /start command",
        'canceled': "Thank you for canceling! Now you can order another pizza"
                    " using /start command",
        'possible_variants': "Please type one of those variants: {variants}"
    }

    def get_message_by_order_state(self, order):
        return self.state_message.get(order.state)

    def __init__(self):
        self.users = dict()

    def cancel_old_order(self, user):
        """
            Change state of last user's order to canceled
            :param user:
        """
        last_order = user.get_last_order()
        if last_order:
            last_order.cancel_order()
        return self.get_message_by_order_state(last_order)

    def user_get_or_create(self, user_id):
        """
            Create User model and save it in manager
            :param user_id: telegram user's id
            :return: User object
        """
        if not self.users.get(user_id):
            user = User(user_id)
            self.users[user_id] = user
            return user
        else:
            return self.users.get(user_id)

    def get_last_question(self, order):
        if getattr(order, 'pizza') and getattr(order, 'payment'):
            last_question = self.get_message_by_order_state(order) \
                .format(
                pizza_size=order.pizza.pizza_size,
                payment_method=order.payment
            )
        else:
            last_question = self.get_message_by_order_state(order)
        return last_question

    def create_new_order(self, user):
        """
            Check if user has not finished order, than
            system says that user has to continue old order
            or cancel it before create new one.

            :return: tuple with new order/error_message, created/not created
        """
        if user.can_create_order():
            order = Order()
            Machine(model=order,
                    states=PizzaStateMachineDescription.states,
                    transitions=PizzaStateMachineDescription.transitions,
                    initial='initial')
            user.orders.append(order)
        else:
            last_order = user.get_last_order()
            last_question = self.get_last_question(last_order)
            raise PizzaServiceException(
                'Please finish or cancel (using /cancel command) your '
                'last order before start new one. {}'.format(last_question)
            )
        return order

    def change_order_state(self, user, user_answer):
        """
            Get user's last order and change the state by user's answer
            :return: last order state's message for user
        """

        last_order = user.get_last_order()

        # get all possible variants of answer for current order state
        input_variants = PizzaStateMachineDescription.variants.get(last_order.state)
        # get answer variant by user typed answer
        user_input = input_variants.get(user_answer.lower())

        if user_input:
            if last_order.is_initial():
                # from initial to pizza_selected
                last_order.set_pizza(user_input)
            elif last_order.is_pizza_selected():
                # from pizza_selected to payment_selected
                last_order.set_payment(user_input)
            elif last_order.is_payment_selected():
                # from payment_selected to success
                last_order.confirm_order(user_input)
                if last_order.confirmed == CONFIRMED:
                    last_order.confirm()
                else:
                    last_order.cancel_order()

            return self.get_message_by_order_state(last_order).format(
                pizza_size=last_order.pizza.pizza_size,
                payment_method=last_order.payment
            )
        else:
            raise PizzaServiceException(
                self.state_message.get('possible_variants').format(
                    variants=', '.join(list(input_variants))))
