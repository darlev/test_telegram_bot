PIZZA_SMALL_SIZE = 1
PIZZA_BIG_SIZE = 2

PAYMENT_BY_CARD = 1
PAYMENT_BY_CASH = 2

CONFIRMED = 1
CANCELED = 2


class PizzaStateMachineDescription(object):
    """
        Configs for state machine that describes order of pizza
    """
    states = [
        'initial',
        'pizza_selected',
        'payment_selected',
        'success',
        'canceled',
    ]

    transitions = [
        {
            'trigger': 'set_pizza',
            'source': 'initial',
            'dest': 'pizza_selected',
            'before': 'select_pizza'
        },
        {
            'trigger': 'set_payment',
            'source': 'pizza_selected',
            'dest': 'payment_selected',
            'before': 'select_payment',
        },
        {
            'trigger': 'confirm',
            'source': 'payment_selected',
            'dest': 'success',
        },
        {
            'trigger': 'cancel_order',
            'source': '*',
            'dest': 'canceled'
        }
    ]

    variants = {
        'initial': {
            'small': PIZZA_SMALL_SIZE,
            'big': PIZZA_BIG_SIZE
        },
        'pizza_selected': {
            'cash': PAYMENT_BY_CASH,
            'card': PAYMENT_BY_CARD
        },
        'payment_selected': {
            'yes': CONFIRMED,
            'no': CANCELED
        },
    }
