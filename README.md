# README #

Test telegram bot for order pizza using:

* Python version: 3.6
* python-telegram-bot==10.0.2
* transitions==0.6.5

### How to configure project? ###

There is file `config.py` with all available configs for current project.
There you can find DEBUG configurations:

* if DEBUG = False => small http server will be started for listen webhooks
* if DEBUG = True => bot listen updates using short polling

### How do I get set up? ###

```$ python run_bot.py```
 
! Please don't forget use correct virtual environment

### How can I communicate with bot? ###

Bot is deployed on heroku server. For use it open your web telegram 
or telegram app, find @PyPizzaBot and press button `Start`

### How can I run test? ###

```$ python -m unittest ```

! Please don't forget use correct virtual environment
