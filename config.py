"""
    File with configuration for all settings using by bot.
"""
import os
# username for bot: PyPizzaBot
# bot name: pizza_bot_test

PYPIZZA_BOT_TOKEN = '575734690:AAEEsF-XSzoFozVbZfL6trnkgZ5g2wj_WdE'

# depends on CPU of the server
WORKER_COUNT = os.cpu_count() * 2

SOCIAL_BOT_MANAGERS = {
    'telegram': 'pypizza_bot.telegram_manager.TelegramHandler',
}

HEROKU_URL = 'https://young-refuge-46052.herokuapp.com/'
LISTEN_IP = '0.0.0.0'
LISTEN_PORT = 8443

DEBUG = False
