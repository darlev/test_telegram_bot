from importlib import import_module


def import_cls_by_string(path):
    """
    :param path: path to module from config
    :return: bot class from given module
    """
    bot_manager_path = path.split('.')
    manager_class = bot_manager_path[-1]

    module = import_module('.'.join(bot_manager_path[:-1]))
    return getattr(module, manager_class)
