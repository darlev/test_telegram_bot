import logging

import config
from pizza_state_machine.service import PizzaService
from utils import import_cls_by_string

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG)

if __name__ == '__main__':
    # get manager for communicate with Telegram
    telegram_handler_path = config.SOCIAL_BOT_MANAGERS.get('telegram')
    pizza_serive = PizzaService()
    pizza_bot_handler = import_cls_by_string(telegram_handler_path)(pizza_serive)
    pizza_bot_handler.run_bot()
