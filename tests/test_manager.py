import unittest
from unittest.mock import MagicMock, patch

from pypizza_bot.telegram_manager import TelegramHandler


class TelegramHandlerTest(unittest.TestCase):
    def setUp(self):
        self.pizza_service = MagicMock()
        self.update = MagicMock()
        self.bot = MagicMock()

    def test_telegram_handler_interface(self):
        telegram_manager = TelegramHandler(self.pizza_service)

        self.assertTrue(hasattr(telegram_manager, 'start_command'))

        self.assertTrue(hasattr(telegram_manager, 'cancel_command'))

        self.assertTrue(hasattr(telegram_manager, 'handle_message'))

    def test_start_command(self):

        with patch.object(TelegramHandler, 'start_command',
                          return_value=None) as start_mock:
            obj = TelegramHandler(self.pizza_service)
            obj.start_command(self.bot, self.update)
        start_mock.assert_called_once_with(self.bot, self.update)

    def test_cancel_command(self):
        with patch.object(TelegramHandler, 'cancel_command',
                          return_value=None) as cancel_mock:
            obj = TelegramHandler(self.pizza_service)
            obj.cancel_command(self.bot, self.update)
        cancel_mock.assert_called_once_with(self.bot, self.update)

    def test_handle_message(self):
        with patch.object(TelegramHandler, 'handle_message',
                          return_value=None) as handle_message_mock:
            obj = TelegramHandler(self.pizza_service)
            obj.handle_message(self.bot, self.update, {})
        handle_message_mock.assert_called_once_with(self.bot, self.update, {})


