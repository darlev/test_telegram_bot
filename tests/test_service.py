import unittest
from random import randint

from pizza_state_machine.models import User, Order
from pizza_state_machine.service import PizzaService, PizzaServiceException
from pizza_state_machine.state_machine_description import PAYMENT_BY_CASH, \
    PIZZA_BIG_SIZE, CONFIRMED


class PizzaServiceTestCase(unittest.TestCase):
    """
        Unit Tests for Pizza service and model methods
    """

    def setUp(self):
        self.user = User(randint(0, 9))
        self.pizza_service = PizzaService()

    def test_get_or_create_new_user(self):
        user = self.pizza_service.user_get_or_create(randint(0, 9))

        self.assertIsInstance(user, User)
        self.assertListEqual(user.orders, [])

    def test_create_new_order(self):
        order = self.pizza_service.create_new_order(self.user)

        # check if returned obj type is Order
        self.assertIsInstance(order, Order)
        # check if returned obj was saved in user's order list
        self.assertIn(order, self.user.orders)

        # try create new one order
        with self.assertRaises(PizzaServiceException):
            order_1 = self.pizza_service.create_new_order(self.user)

    def test_cancel_old_order(self):
        order = self.pizza_service.create_new_order(self.user)

        with self.assertRaises(PizzaServiceException):
            order_1 = self.pizza_service.create_new_order(self.user)

        order.cancel_order()
        self.assertEqual(order.state, 'canceled')

        order_1 = self.pizza_service.create_new_order(self.user)

        self.assertIn(order_1, self.user.orders)

    def test_change_order_state(self):
        order = self.pizza_service.create_new_order(self.user)

        self.assertEqual(order.state, 'initial')

        user_input = 'dshggs'

        with self.assertRaises(PizzaServiceException):
            self.pizza_service.change_order_state(self.user, user_input)

        user_input = 'big'
        self.pizza_service.change_order_state(self.user, user_input)

        self.assertEqual(order.state, 'pizza_selected')
        self.assertEqual(order.pizza.size, PIZZA_BIG_SIZE)

        user_input = 'sdlgd'

        with self.assertRaises(PizzaServiceException):
            self.pizza_service.change_order_state(self.user, user_input)

        user_input = 'cash'
        self.pizza_service.change_order_state(self.user, user_input)

        self.assertEqual(order.state, 'payment_selected')
        self.assertEqual(order.payment_method, PAYMENT_BY_CASH)

        user_input = 'aaaaa'

        with self.assertRaises(PizzaServiceException):
            self.pizza_service.change_order_state(self.user, user_input)

        user_input = 'yes'
        self.pizza_service.change_order_state(self.user, user_input)

        self.assertEqual(order.state, 'success')
        self.assertEqual(order.confirmed, CONFIRMED)
