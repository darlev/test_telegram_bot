import unittest
from random import randint

from transitions.core import Machine

from pizza_state_machine.models import User, Order, Pizza
from pizza_state_machine.state_machine_description import \
    PizzaStateMachineDescription, PIZZA_SMALL_SIZE, PAYMENT_BY_CASH


class PizzaModelTestCase(unittest.TestCase):

    def test_pizza_creation(self):
        with self.assertRaises(TypeError):
            pizza = Pizza()

        size_variants = ['big', 'small']
        initial_variants = PizzaStateMachineDescription.variants.get('initial')
        for size in size_variants:
            with self.subTest('Test create pizza with different sizes'):
                size_variant = initial_variants.get(size)
                pizza = Pizza(size_variant)
                self.assertEqual(pizza.pizza_size, size)

        size_variants = ['dsg', 'sfasf', 0, 4]
        for size in size_variants:
            with self.subTest('Test create pizza with different sizes'):
                with self.assertRaises(TypeError):
                    pizza = Pizza(size)


class OrderModelTestCase(unittest.TestCase):

    def test_order_creation(self):
        order = Order()

        self.assertEqual(order.confirmed, False)
        self.assertEqual(order.payment_method, None)
        self.assertEqual(order.pizza, None)

    def test_select_pizza(self):
        order = Order()
        self.assertIsNone(order.pizza)

        pizza_size = 'asfsdf'

        with self.assertRaises(TypeError):
            order.select_pizza(pizza_size)

        order.select_pizza(PIZZA_SMALL_SIZE)
        self.assertIsNotNone(order.pizza)

    def test_select_payment(self):
        order = Order()

        self.assertIsNone(order.payment_method)

        with self.assertRaises(TypeError):
            order.select_payment('payment_method')

        order.select_payment(PAYMENT_BY_CASH)
        self.assertIsNotNone(order.payment_method)
        self.assertIsNotNone(order.payment)

    def test_confirm_order(self):
        order = Order()
        self.assertFalse(order.confirmed)

        with self.assertRaises(TypeError):
            order.confirm_order('sadfasf')

        order.confirm_order(True)
        self.assertTrue(order.confirmed)


class UserModelTestCase(unittest.TestCase):

    def setUp(self):
        self.user = User(randint(0, 9))

    def test_can_create_order(self):
        # create new order with success status
        order = Order()
        Machine(model=order,
                states=PizzaStateMachineDescription.states,
                transitions=PizzaStateMachineDescription.transitions,
                initial='initial')
        self.user.orders.append(order)

        order.to_success()
        can_create_order = self.user.can_create_order()

        self.assertTrue(can_create_order)

        # create new order with canceled status
        order = Order()
        Machine(model=order,
                states=PizzaStateMachineDescription.states,
                transitions=PizzaStateMachineDescription.transitions,
                initial='initial')
        self.user.orders.append(order)

        order.to_canceled()
        can_create_order = self.user.can_create_order()

        self.assertTrue(can_create_order)

        # create new order with pizza_selected status
        order = Order()
        Machine(model=order,
                states=PizzaStateMachineDescription.states,
                transitions=PizzaStateMachineDescription.transitions,
                initial='initial')
        self.user.orders.append(order)

        order.to_pizza_selected()
        can_create_order = self.user.can_create_order()

        self.assertFalse(can_create_order)
