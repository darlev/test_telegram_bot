import os

from telegram.ext import Updater
from telegram.ext.commandhandler import CommandHandler
from telegram.ext.filters import Filters
from telegram.ext.messagehandler import MessageHandler

import config

import logging

from pizza_state_machine.service import PizzaService, PizzaServiceException
from pypizza_bot.handlers import BaseChatHandler

logger = logging.getLogger(__name__)


class TelegramHandler(BaseChatHandler):
    """
        Handler for manage work of telegram bot,
        that implements all abstract methods from BaseChatHandler.
        It's goal is sending message to user in telegram, listening updates,
        create order etc.
    """

    def __init__(self, pizza_service):
        # create updater for manage job queue
        self.updater = Updater(token=config.PYPIZZA_BOT_TOKEN,
                               workers=config.WORKER_COUNT)
        # initialize message handler for receive and send message
        message_handler = MessageHandler(filters=Filters.all,
                                         callback=self.handle_message,
                                         pass_chat_data=True)
        # initialize start command handler
        start_command_handler = CommandHandler(command="start",
                                               callback=self.start_command)

        # initialize cancel command handler
        cancel_command_handler = CommandHandler(command="cancel",
                                                callback=self.cancel_command)

        # register handlers
        self.updater.dispatcher.add_handler(start_command_handler)
        self.updater.dispatcher.add_handler(cancel_command_handler)
        self.updater.dispatcher.add_handler(message_handler)

        # set the first offset for job
        self.offset_for_update = None

        # init users and orders manager
        self.pizza_service = pizza_service

    def cancel_command(self, bot, update):
        user = self.pizza_service.user_get_or_create(update.effective_user.id)
        logger.debug("Cancel last order for user {}".format(user))
        reply_text = self.pizza_service.cancel_old_order(user)
        update.message.reply_text(reply_text)

    def start_command(self, bot, update):
        user = self.pizza_service.user_get_or_create(update.effective_user.id)
        logger.debug("User {} started new chat with telegram bot.".format(user))
        try:
            order = self.pizza_service.create_new_order(user)
            text_message = self.pizza_service.get_message_by_order_state(order)
        except PizzaServiceException as ex:
            logger.debug("Got error while creation new order")
            text_message = str(ex)
        update.message.reply_text(text_message)

    def handle_message(self, bot, update, chat_data, **kwargs):
        user = self.pizza_service.user_get_or_create(update.effective_user.id)
        logger.debug("User {} answered {}".format(user, update.message))

        try:
            reply_text = self.pizza_service.change_order_state(
                user, update.message['text'])
        except PizzaServiceException as ex:
            logger.debug("Got error while handling message")
            reply_text = str(ex)
        except AttributeError:
            reply_text = "Want to start? /start"

        self.updater.last_update_id = update.update_id + 1
        self.updater.bot.send_message(chat_id=update.message.chat_id,
                                      text=reply_text)

    def run_bot(self):
        if config.DEBUG:
            logger.debug("Start polling updates from Telegram")
            self.job_queue = self.updater.start_polling()
        else:
            logger.debug("Start http server for listen updates via webhook")
            logger.debug("Port: {}".format(os.environ.get('PORT')))
            PORT = int(os.environ.get('PORT', config.LISTEN_PORT))
            self.job_queue = self.updater.start_webhook(
                listen=config.LISTEN_IP,
                port=PORT,
                url_path=config.PYPIZZA_BOT_TOKEN

            )
            webhook_url = config.HEROKU_URL + config.PYPIZZA_BOT_TOKEN
            self.updater.bot.set_webhook(url=webhook_url)
            self.updater.idle()
        logger.debug("Job Queue: {}".format(self.job_queue))

    def stop_bot(self):
        logger.debug("Stop bot running")
        self.updater.stop()
