from abc import abstractmethod


class BaseChatHandler(object):
    """
        Abstract Base handler for manage bot's work for different
        social network as telegram, facebook, skype etc.
    """

    @abstractmethod
    def start_command(self, *args, **kwargs):
        """Send a message when the command /start is issued."""
        raise NotImplementedError()

    @abstractmethod
    def handle_message(self, *args, **kwargs):
        """Send a message when text message is issued."""
        raise NotImplementedError()

    @abstractmethod
    def run_bot(self, *args, **kwargs):
        """Start long polling updates from handler API."""
        raise NotImplementedError()

    @abstractmethod
    def stop_bot(self, *args, **kwargs):
        """Stop polling updates from handler API."""
        raise NotImplementedError()
